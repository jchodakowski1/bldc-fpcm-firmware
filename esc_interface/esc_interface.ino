// Brushless Fuel Systems
// ESC Interface Firmware
// 05/12/2019
// v1.0

#include "esc.h"
#include <EEPROM.h>


// Firmware Version
#define FIRMWARE_VERSION            "FPCM-01-051219"


// Hardware Defaults
#define ESC_ADDRESS                 0x29                          // 0x29 thru 0x39
#define SLAVE_SENSE_PIN             2                             // Slave connected pin
#define PWM_INPUT_PIN               3                             // PWM Read pin
#define PWM_OUTPUT_PIN              4                             // PWM Output pin for daisy-chain function
#define PWM_UPDATE_INTERVAL         50                            // (ms) Update PWM Hz and percent this often
#define PWM_TIMEOUT                 2000000                       // (us) If no valid pulses within this period, disable pump
#define BAD_PWM_LIMIT               20                            // After pwm frequency flagged out of range this many times, disable pump
#define PWM_VALID_LOW               5                             // PWM duty below this will be ignored
#define PWM_VALID_HIGH              95                            // PWM duty above this will be ignored
#define SERIAL_UPDATE_INTERVAL      100                           // (ms) Update data buffer, motor data, and check for serial query this often


// Global Variables
I2C_ESC                   motor(ESC_ADDRESS);                     // ESC Object
unsigned long             timeNow;                                // (ms) Current loop time (((((MOVE TO LOCAL???)))))
unsigned long             updateTimer;                            // (ms) Keep track of when to update PWM frequency & duty cycle
unsigned long             startTimer;                             // (ms) Keep track of what time we started the pump
unsigned long             serialTimer;                            // (ms) Keep track of what time serial data was last updated
unsigned long             refreshTimer;                           // (ms) attempt to fix reset
byte                      badPWMCount;                            // Increments every time the pwm is outside the input specification
uint16_t                  pwmHz;                                  // Input frequency
uint16_t                  pwmPercentRead;                         // PWM percent as read * 10
byte                      pwmPercent;                             // PWM percent as calculated by HIGH/LOW
long                      outputCommand;                          // Smoothed output command to ESC
long                      outputCommandRequest;                   // Calculated output command
volatile unsigned long    pwmPulseTime;                           // (us) Last time the pin changed state
volatile unsigned long    pwmPulseTimeLow;                        // (us) Last time the pin went from high to low, falling edge
volatile unsigned long    pwmPulseLength;                         // (us) Calculated length of pulse based on rising edge time minus last falling edge time
volatile unsigned long    pwmPeriod;                              // (us) Calculated time between 2 same falling edges
volatile byte             dataBuffer[100];                        // Serial data buffer


// Settings
uint16_t                  outputCommandOverride     = 0;          // Set motor override command to 0-32767
uint16_t                  inputFrequency            = 100;        // Input frequency (Hz)
byte                      inputFrequencyDeadband    = 5;          // Signal must be within this range +/- of input frequency
uint16_t                  startCommand              = 10000;      // Startup at this speed to ensure clean start
uint16_t                  startTime                 = 500;        // (ms) Startup mode activated for this long
byte                      increasingFilter          = 25;         // Number to add to pump command each cycle to raise output
byte                      decreasingFilter          = 5;          // Number to subtract from pump command each cycle to lower output
byte                      lowSpeedPWMin             = 20;         // Lowest PWM Input signal
uint16_t                  lowSpeedOutput            = 4000;       // Lowest motor speed output
byte                      highSpeedPWMin            = 75;         // Highest PWM Input signal
uint16_t                  highSpeedOutput           = 32767;      // Highest motor speed command
unsigned long             serialNumber              = 0;          // Serial number
unsigned long             pwmInputPeriod            = 10000;      // (uS) Input frequency period
unsigned long             pwmInputPeriodDeadband    = 100;        // (uS) Input frequency period deadband


void setup() {
  // Allow the ESC to come online before continuing or I2C communication will fail
  delay(100);
  Wire.begin();

  // Configure input/output pins
  pinMode(SLAVE_SENSE_PIN, INPUT_PULLUP);
  pinMode(PWM_OUTPUT_PIN, OUTPUT);
  pinMode(PWM_INPUT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(PWM_INPUT_PIN), triggerPWM, CHANGE);

  // Read settings from EEPROM into global variables
  readSettingsFromEEPROM();

  // Begin serial communications
  Serial.begin(38400);

  // Arm ESC before sending speed commands
  motor.set(0);
}


void loop() {
  timeNow = millis();

  // Calculate input frequency
  if (pwmPeriod > 0) {
    pwmHz = 1000000 / pwmPeriod;
  }

  // Update PWM Period and deadband
  pwmInputPeriod = 1000000 / max(inputFrequency, 1);  // Prevent divide by zero when in hi-low mode
  pwmInputPeriodDeadband = pwmInputPeriod * (inputFrequencyDeadband / 100.0);

  // Update PWM Percent
  if (timeNow >= (updateTimer + PWM_UPDATE_INTERVAL)) {
    // Check if in range of input frequency, if not, start flagging bad signal
    if ((pwmPeriod <= (pwmInputPeriod + pwmInputPeriodDeadband)) && (pwmPeriod >= (pwmInputPeriod - pwmInputPeriodDeadband))) {
      pwmPercentRead = (1000 * pwmPulseLength) / pwmPeriod;
      badPWMCount = 0;
    }
    else {
      badPWMCount++;
    }
    updateTimer = timeNow;
  }

  // If PWM signal has been missing for timeout value, then disable pump
  if ((timeNow * 1000) >= pwmPulseTime + PWM_TIMEOUT) {
    pwmHz = 0;
    pwmPercentRead = 0;
  }

  // If percentage is within valid range and bad signal hasnt exceeded limit, constrain input percent and translate to output command, otherwise 0
  if ((pwmPercentRead >= (PWM_VALID_LOW * 10)) && (pwmPercentRead <= (PWM_VALID_HIGH * 10)) && (badPWMCount < BAD_PWM_LIMIT)) {
    pwmPercent = constrain((pwmPercentRead / 10), lowSpeedPWMin, highSpeedPWMin);
    outputCommandRequest = map(pwmPercent, lowSpeedPWMin, highSpeedPWMin, lowSpeedOutput, highSpeedOutput);
  }
  else {
    pwmPercent = 0;
    outputCommandRequest = 0;
  }

  // If debug mode is set, override PWM input
  if (outputCommandOverride > 0) {
    outputCommandRequest = outputCommandOverride;
  }

  // Low-High mode, PWM pin open circuit use low speed (or off), and grounded high speed
  if ((timeNow > 1000) && (inputFrequency == 0)) {
    if (digitalRead(PWM_INPUT_PIN) == HIGH) {
      outputCommandRequest = lowSpeedOutput;
    }
    else {
      outputCommandRequest = highSpeedOutput;
    }
  }

  if (outputCommandRequest == 0) {
    startTimer = timeNow;
  }
  
  if ((outputCommandRequest > 0) && (timeNow < startTimer + startTime)) {
    outputCommandRequest = startCommand;
  }

  if (outputCommandRequest > (outputCommand + increasingFilter)) {
    outputCommand += increasingFilter;
  }
  else if (outputCommandRequest < (outputCommand - decreasingFilter)) {
    outputCommand -= decreasingFilter;
  }
  else {
    outputCommand = outputCommandRequest;
  }
  outputCommand = constrain(outputCommand, 0, 32767);


  if (timeNow >= (refreshTimer + 250)) {
    refreshTimer = timeNow;
  }
  else {
    // Tell pump to run
    motor.set(outputCommand);
  }

  // Update motor data, update data buffer, check for serial comms
  if (timeNow >= (serialTimer + SERIAL_UPDATE_INTERVAL)) {
    motor.update();
    stuffData();
    serialTimer = timeNow;
  }

  doSerial();
}


// Interrupt service routine when PWM input pin changes state
void triggerPWM() {
  pwmPulseTime = micros();

  if (digitalRead(PWM_INPUT_PIN) == LOW) {
    // Falling Edge
    pwmPeriod = pwmPulseTime - pwmPulseTimeLow;
    pwmPulseTimeLow = pwmPulseTime;
  }
  else {
    // Rising Edge
    pwmPulseLength = pwmPulseTime - pwmPulseTimeLow;
  }
}


// Insert realtime data into data buffer
void stuffData() {
  // Bitstuff codes and status bits
  if (motor.isAlive()) {
    dataBuffer[0] = 1;
  }
  else {
    dataBuffer[0] = 0;
  }

  // RPM
  uint16_t rpm = motor.rpm();
  dataBuffer[1] = rpm & 0xFF;
  dataBuffer[2] = (rpm >> 8) & 0xFF;

  // Voltage
  int volts = int(motor.voltage() * 10);
  dataBuffer[3] = constrain(volts, 0, 0xFF);

  // Current
  int current = int(motor.current() * 10);
  dataBuffer[4] = constrain(current, 0, 0xFF);

  // Temperature
  int temp = int(motor.temperature());
  dataBuffer[5] = constrain(temp, 0, 0xFF);

  // PWM Frequency
  dataBuffer[6] = pwmHz & 0xFF;
  dataBuffer[7] = (pwmHz >> 8) & 0xFF;

  // PWM Percent Read
  int pwmpctread = pwmPercentRead / 10;
  dataBuffer[8] = pwmpctread & 0xFF;

  // Output Command
  dataBuffer[9] = outputCommand & 0xFF;
  dataBuffer[10] = (outputCommand >> 8) & 0xFF;
  dataBuffer[11] = (outputCommand >> 16) & 0xFF;
  dataBuffer[12] = (outputCommand >> 24) & 0xFF;

  // Output Command Request
  dataBuffer[13] = outputCommandRequest & 0xFF;
  dataBuffer[14] = (outputCommandRequest >> 8) & 0xFF;
  dataBuffer[15] = (outputCommandRequest >> 16) & 0xFF;
  dataBuffer[16] = (outputCommandRequest >> 24) & 0xFF;

  // PWM Percent
  dataBuffer[17] = pwmPercent & 0xFF;

  // Bad PWM Count
  dataBuffer[18] = badPWMCount & 0xFF;
  
  // PWM Pulse Time
  dataBuffer[19] = pwmPulseTime & 0xFF;
  dataBuffer[20] = (pwmPulseTime >> 8) & 0xFF;
  dataBuffer[21] = (pwmPulseTime >> 16) & 0xFF;
  dataBuffer[22] = (pwmPulseTime >> 24) & 0xFF;

  // PWM Pulse Time Low
  dataBuffer[23] = pwmPulseTimeLow & 0xFF;
  dataBuffer[24] = (pwmPulseTimeLow >> 8) & 0xFF;
  dataBuffer[25] = (pwmPulseTimeLow >> 16) & 0xFF;
  dataBuffer[26] = (pwmPulseTimeLow >> 24) & 0xFF;

  // PWM Pulse Length
  dataBuffer[27] = pwmPulseLength & 0xFF;
  dataBuffer[28] = (pwmPulseLength >> 8) & 0xFF;
  dataBuffer[29] = (pwmPulseLength >> 16) & 0xFF;
  dataBuffer[30] = (pwmPulseLength >> 24) & 0xFF;

  // PWM Period
  dataBuffer[31] = pwmPeriod & 0xFF;
  dataBuffer[32] = (pwmPeriod >> 8) & 0xFF;
  dataBuffer[33] = (pwmPeriod >> 16) & 0xFF;
  dataBuffer[34] = (pwmPeriod >> 24) & 0xFF;

  // PWM Input Period
  dataBuffer[35] = pwmInputPeriod & 0xFF;
  dataBuffer[36] = (pwmInputPeriod >> 8) & 0xFF;
  dataBuffer[37] = (pwmInputPeriod >> 16) & 0xFF;
  dataBuffer[38] = (pwmInputPeriod >> 24) & 0xFF;

  // PWM Input Period Deadband
  dataBuffer[39] = pwmInputPeriodDeadband & 0xFF;
  dataBuffer[40] = (pwmInputPeriodDeadband >> 8) & 0xFF;
  dataBuffer[41] = (pwmInputPeriodDeadband >> 16) & 0xFF;
  dataBuffer[42] = (pwmInputPeriodDeadband >> 24) & 0xFF;


  // Time Now
  dataBuffer[43] = timeNow & 0xFF;
  dataBuffer[44] = (timeNow >> 8) & 0xFF;
  dataBuffer[45] = (timeNow >> 16) & 0xFF;
  dataBuffer[46] = (timeNow >> 24) & 0xFF;

  // Update Timer
  dataBuffer[47] = updateTimer & 0xFF;
  dataBuffer[48] = (updateTimer >> 8) & 0xFF;
  dataBuffer[49] = (updateTimer >> 16) & 0xFF;
  dataBuffer[50] = (updateTimer >> 24) & 0xFF;

  // Start Timer
  dataBuffer[51] = startTimer & 0xFF;
  dataBuffer[52] = (startTimer >> 8) & 0xFF;
  dataBuffer[53] = (startTimer >> 16) & 0xFF;
  dataBuffer[54] = (startTimer >> 24) & 0xFF;
}


void doSerial() {
  if(Serial.available() > 0) {
    byte readByte = Serial.read();

    switch(readByte) {
      case 0x46: // 'F'  - Firmware version
        Serial.print(FIRMWARE_VERSION);
        break;

      case 0x53: // 'S' - Serial Number
        Serial.print(serialNumber);
        break;
      
      case 0x41: // 'A' - Live data, group A
        for (int i = 0; i <= 5; i++) {
          Serial.write(dataBuffer[i]);
        }
        break;
        
      case 0x42: // 'B' - Live data, group B
        for (int i = 6; i <= 12; i++) {
          Serial.write(dataBuffer[i]);
        }
        break;
        
      case 0x43: // 'C' - Live data, group C
        for (int i = 13; i <= 54; i++) {
          Serial.write(dataBuffer[i]);
        }
        break;

      case 0x44: // 'D' - Restore dactory default settings
        // return 'd' after restored
        break;

      case 0x55: // 'U' - Update setting
        byte settingIndex;
        byte settingData[6];    // First 4 bytes are data, last 2 are crc
        uint16_t checkCRC;
        byte checkCRCHigh;
        byte checkCRCLow;
        
        delay(20);
        settingIndex = Serial.read();

        for (int x = 0; x < 6; x++) {
          delay(20);
          settingData[x] = Serial.read();
        }

        checkCRC = CRC16_MODBUS(settingData);
        checkCRCHigh = checkCRC >> 8;
        checkCRCLow = checkCRC & 0xFF;
        
        if ((checkCRCHigh == settingData[4]) && (checkCRCLow == settingData[5])) {
          Serial.write(0x47); // "G" - CRC16 passed
          
          byte validate8;
          uint16_t validate16;
          uint32_t validate32;
          
          switch (settingIndex) {
            case 0x30:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 32767) {
                outputCommandOverride = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x31:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 1000) {
                inputFrequency = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x32:
              validate8 = settingData[3];
              if (validate8 >= 1 && validate8 <= 25) {
                inputFrequencyDeadband = validate8;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x33:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 32767) {
                startCommand = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x34:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 32767) {
                startTime = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x35:
              validate8 = settingData[3];
              if (validate8 >= 1 && validate8 <= 50) {
                increasingFilter = validate8;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x36:
              validate8 = settingData[3];
              if (validate8 >= 1 && validate8 <= 50) {
                decreasingFilter = validate8;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x37:
              validate8 = settingData[3];
              if (validate8 >= 5 && validate8 <= 95) {
                lowSpeedPWMin = validate8;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x38:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 32767) {
                lowSpeedOutput = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x39:
              validate8 = settingData[3];
              if (validate8 >= 5 && validate8 <= 95) {
                highSpeedPWMin = validate8;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x3A:
              validate16 = (settingData[2] << 8) + settingData[3];
              if (validate16 >= 0 && validate16 <= 32767) {
                highSpeedOutput = validate16;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            case 0x3B:
              validate32 = ((uint32_t)settingData[0] << 24) + ((uint32_t)settingData[1] << 16) + ((uint32_t)settingData[2] << 8) + (uint32_t)settingData[3];
              if (validate32 >= 0 && validate32 <= 999999999) {
                serialNumber = validate32;
                Serial.write(0x56);
              }
              else {
                Serial.write(0x49);
              }
              break;

            default:
              break;
          }
        }
        else {
          Serial.write(0x45); // "E" - CRC16 or data transmission failure
        }
        break;

      case 0x5A: // 'Z' - Read current settings
        Serial.write(outputCommandOverride & 0xFF);
        Serial.write((outputCommandOverride >> 8) & 0xFF);
        Serial.write(inputFrequency & 0xFF);
        Serial.write((inputFrequency >> 8) & 0xFF);
        Serial.write(inputFrequencyDeadband & 0xFF);
        Serial.write(startCommand & 0xFF);
        Serial.write((startCommand >> 8) & 0xFF);
        Serial.write(startTime & 0xFF);
        Serial.write((startTime >> 8) & 0xFF);
        Serial.write(increasingFilter & 0xFF);
        Serial.write(decreasingFilter & 0xFF);
        Serial.write(lowSpeedPWMin & 0xFF);
        Serial.write(lowSpeedOutput & 0xFF);
        Serial.write((lowSpeedOutput >> 8) & 0xFF);
        Serial.write(highSpeedPWMin & 0xFF);
        Serial.write(highSpeedOutput & 0xFF);
        Serial.write((highSpeedOutput >> 8) & 0xFF);
        break;

      case 0x58: // 'X' - Burn Settings to EEPROM
        int eepromResult;
        eepromResult = storeSettingsToEEPROM();
        delay(50);
        if (eepromResult) {
          Serial.write(0x59);
        }
        else {
          Serial.write(0x79);
        }
        break;
        
      default:
        break;
    }
  }
}


// Reads all settings from EEPROM
void readSettingsFromEEPROM() {
  uint16_t _inputFrequency;
  byte _inputFrequencyDeadband;
  uint16_t _startCommand;
  uint16_t _startTime;
  byte _increasingFilter;
  byte _decreasingFilter;
  byte _lowSpeedPWMin;
  uint16_t _lowSpeedOutput;
  byte _highSpeedPWMin;
  uint16_t _highSpeedOutput;
  uint32_t _serialNumber;

  EEPROM.get(110, _inputFrequency);
  EEPROM.get(120, _inputFrequencyDeadband);
  EEPROM.get(130, _startCommand);
  EEPROM.get(140, _startTime);
  EEPROM.get(150, _increasingFilter);
  EEPROM.get(160, _decreasingFilter);
  EEPROM.get(170, _lowSpeedPWMin);
  EEPROM.get(180, _lowSpeedOutput);
  EEPROM.get(190, _highSpeedPWMin);
  EEPROM.get(200, _highSpeedOutput);
  EEPROM.get(210, _serialNumber);

  if (_inputFrequency >= 0 && _inputFrequency <= 1000) {
    inputFrequency = _inputFrequency;
  }

  if (_inputFrequencyDeadband >= 1 && _inputFrequencyDeadband <= 25) {
    inputFrequencyDeadband = _inputFrequencyDeadband;
  }

  if (_startCommand >= 0 && _startCommand <= 32767) {
    startCommand = _startCommand;
  }

  if (_startTime >= 0 && _startTime <= 32767) {
    startTime = _startTime;
  }

  if (_increasingFilter >= 1 && _increasingFilter <= 50) {
    increasingFilter = _increasingFilter;
  }

  if (_decreasingFilter >= 1 && _decreasingFilter <= 50) {
    decreasingFilter = _decreasingFilter;
  }

  if (_lowSpeedPWMin >= 5 && _lowSpeedPWMin <= 95) {
    lowSpeedPWMin = _lowSpeedPWMin;
  }

  if (_lowSpeedOutput >= 0 && _lowSpeedOutput <= 32767) {
    lowSpeedOutput = _lowSpeedOutput;
  }

  if (_highSpeedPWMin >= 5 && _highSpeedPWMin <= 95) {
    highSpeedPWMin = _highSpeedPWMin;
  }

  if (_highSpeedOutput >= 0 && _highSpeedOutput <= 32767) {
    highSpeedOutput = _highSpeedOutput;
  }

  if (_serialNumber >= 0 && _serialNumber <= 999999999) {
    serialNumber = _serialNumber;
  }
}


// Writes all settings into EEPROM
int storeSettingsToEEPROM() {
  uint16_t _inputFrequency;
  byte _inputFrequencyDeadband;
  uint16_t _startCommand;
  uint16_t _startTime;
  byte _increasingFilter;
  byte _decreasingFilter;
  byte _lowSpeedPWMin;
  uint16_t _lowSpeedOutput;
  byte _highSpeedPWMin;
  uint16_t _highSpeedOutput;
  uint32_t _serialNumber;
  
  EEPROM.put(110, inputFrequency);
  EEPROM.put(120, inputFrequencyDeadband);
  EEPROM.put(130, startCommand);
  EEPROM.put(140, startTime);
  EEPROM.put(150, increasingFilter);
  EEPROM.put(160, decreasingFilter);
  EEPROM.put(170, lowSpeedPWMin);
  EEPROM.put(180, lowSpeedOutput);
  EEPROM.put(190, highSpeedPWMin);
  EEPROM.put(200, highSpeedOutput);
  EEPROM.put(210, serialNumber);

  EEPROM.get(110, _inputFrequency);
  EEPROM.get(120, _inputFrequencyDeadband);
  EEPROM.get(130, _startCommand);
  EEPROM.get(140, _startTime);
  EEPROM.get(150, _increasingFilter);
  EEPROM.get(160, _decreasingFilter);
  EEPROM.get(170, _lowSpeedPWMin);
  EEPROM.get(180, _lowSpeedOutput);
  EEPROM.get(190, _highSpeedPWMin);
  EEPROM.get(200, _highSpeedOutput);
  EEPROM.get(210, _serialNumber);

  if ((inputFrequency == _inputFrequency)
  && (inputFrequencyDeadband == _inputFrequencyDeadband)
  && (startCommand == _startCommand)
  && (startTime == _startTime)
  && (increasingFilter == _increasingFilter)
  && (decreasingFilter == _decreasingFilter)
  && (lowSpeedPWMin == _lowSpeedPWMin)
  && (lowSpeedOutput == _lowSpeedOutput)
  && (highSpeedPWMin == _highSpeedPWMin)
  && (highSpeedOutput == _highSpeedOutput)
  && (serialNumber == _serialNumber))
  {
    return (1);
  }
  else {
    return (0);
  }
}


// CRC-16 MODBUS calculation for 4 bytes of data
uint16_t CRC16_MODBUS(byte* crcData) {
  uint16_t crcFull = 0xFFFF;
  byte crcLSB;

  for (int i = 0; i <= 3; i++) {
    crcFull = crcFull ^ crcData[i];

    for (int j = 0; j <= 7; j++) {
      crcLSB = crcFull & 0x01;
      crcFull = crcFull >> 1;

      if (crcLSB != 0) {
        crcFull = crcFull ^ 0xA001;
      }
    }
  }

  return crcFull;
}
