// Brushless Fuel Systems
// ESC w/ I2C Library
// 02/15/2019


#include <Wire.h>


class I2C_ESC {
  public:
    I2C_ESC(uint8_t address, uint8_t poleCount = 6);
    void set(int16_t throttle);
    void update();
    bool isAlive();
    float voltage();
    float current();
    float temperature();
    int16_t rpm();
  
  private:
    uint8_t _address;
    uint16_t _voltage_raw, _current_raw, _temp_raw;
    int16_t _rpm;
    uint16_t _rpmTimer;
    uint8_t _identifier;
    uint8_t _poleCount;
    static void readBuffer(uint8_t address, uint8_t buffer[]);
    void readSensors(uint8_t address, uint16_t *rpm, uint16_t *vbat, uint16_t *temp, uint16_t *curr);
};

namespace {
  uint8_t _buffer[9];
}

// I2C address (0,1...16 is address 0x29,0x2A...0x39). Optionally accepts pole count for RPM measurements.
I2C_ESC::I2C_ESC(uint8_t address, uint8_t poleCount) {
  _address = address;
  _poleCount = poleCount;
}

// Read the incoming data buffer from an ESC
void I2C_ESC::readBuffer(uint8_t address, uint8_t buffer[]) {
  Wire.beginTransmission(address);
  Wire.write(0x02);         // Data start register
  Wire.endTransmission();
  Wire.requestFrom(address,uint8_t(9));
  uint8_t i = 0;
  while(Wire.available()) {
    buffer[i] = Wire.read();
    i++;
  }
}

// Set motor speed
// Range: 16 bit signed (-32767 to +32767)
void I2C_ESC::set(int16_t throttle) {  
  Wire.beginTransmission(_address);
  Wire.write(0x00);
  Wire.write(throttle>>8);
  Wire.write(throttle);  
  Wire.endTransmission();
}

// The update function reads new data from the ESC. If used, this function
// must be called at least every 65 seconds to prevent 16-bit overflow of 
// the timer keeping track of RPM. Recommended to call this function at 4-10 Hz
void I2C_ESC::update() {  
  _buffer[8] = 0x00;      // Reset last byte so we can check for alive

  readBuffer(_address,_buffer);
  
  _rpm = (_buffer[0] << 8) | _buffer[1];
  _voltage_raw = (_buffer[2] << 8) | _buffer[3];
  _temp_raw = (_buffer[4] << 8) | _buffer[5];
  _current_raw = (_buffer[6] << 8) | _buffer[7];
  _identifier = _buffer[8];

  _rpm = float(_rpm)/((uint16_t(millis())-_rpmTimer)/1000.0f)*60/float(_poleCount);
  _rpmTimer = millis();
}

// Returns true is ESC is alive and communicating on I2C Bus
bool I2C_ESC::isAlive() {
  return (_identifier == 0xab);
}

// Returns ESC input voltage in volts
// Voltage divider with 3.3k and 18k resistors
float I2C_ESC::voltage() {
  return float(_voltage_raw)/65536.0f*5.0f*6.45f;
}

// Returns ESC amperage as reported by ACS7XX12 in amps
float I2C_ESC::current() {
  return (float(_current_raw)-32767)/65535.0f*5.0f*14.706f;
}

// Returns ESC MOSFET temperature in deg C
float I2C_ESC::temperature() {
  float resistance = 3300/(65535/float(_temp_raw)-1);

  float steinhart;
  steinhart = resistance / 10000;       // (R/Ro)
  steinhart = log(steinhart);           // ln(R/Ro)
  steinhart /= 3900;                    // 1/B * ln(R/Ro)
  steinhart += 1.0 / (25 + 273.15);     // + (1/To)
  steinhart = 1.0 / steinhart;          // Invert
  steinhart -= 273.15;                  // convert to C

  return steinhart;
}

// Returns RPM of motor
int16_t I2C_ESC::rpm() {
  return _rpm;
}
