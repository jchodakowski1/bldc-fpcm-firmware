@echo off
cls
echo Flash and set fuses to ATmega8A
pause
avrdude -c usbtiny -p m8 -b 19200 -U lfuse:w:0x3f:m -U hfuse:w:0xca:m
avrdude -c usbtiny -p m8 -b 19200 -U flash:w:esc.hex:i
pause
cls
echo Flash and set fuses to ATmega328
pause
cd esc_interface
avrdude -c usbtiny -p atmega328p -U lfuse:w:0xFF:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m
avrdude -c usbtiny -p atmega328p -U flash:w:esc_interface.hex
pause