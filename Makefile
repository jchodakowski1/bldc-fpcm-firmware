SHELL = /bin/bash

all:
	@set -o pipefail; avra -fI -o esc.hex -D MOTOR_ID=0 -d $*.obj esc.asm 2>&1 | grep -v 'PRAGMA'
	@rm -f *.obj *.eep.hex *.cof